FROM python:3.12-slim-bookworm

LABEL com.bizztreat.type="Extractor"
LABEL com.bizztreat.purpose="Bizzflow"
LABEL com.bizztreat.component="ex-mssql"
LABEL com.bizztreat.title="MS SQL Server Extractor"

RUN apt-get update && apt-get -y upgrade && apt-get -y install gnupg2 curl

RUN curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor -o /usr/share/keyrings/microsoft-prod.gpg
RUN curl https://packages.microsoft.com/config/debian/12/prod.list | tee /etc/apt/sources.list.d/mssql-release.list

RUN apt-get update && ACCEPT_EULA=Y apt-get -y install msodbcsql18 unixodbc-dev  libgssapi-krb5-2


ADD requirements.txt .

RUN python3 -m pip install -r requirements.txt


ADD src/ /code
WORKDIR /code

ENTRYPOINT ["python3", "-u", "main.py"]
