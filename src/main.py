#!/usr/bin/env python3
from mssql_extractor.extractor import MySQLExtractor


def main():
    """
    Main function to be called if from __main__
    """
    extractor = MySQLExtractor("/config/config.json")
    extractor.extract()


if __name__ == "__main__":
    main()
