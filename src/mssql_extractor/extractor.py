import csv
import logging
import json
import os
import pyodbc
from retry_helper import RetryManager

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class MySQLExtractor:
    BATCH_LINES = 10000

    def __init__(self, config_file_path):
        self.conf = self.load_config(config_file_path)
        self.batch_lines = self.conf.get("chunk_size", self.BATCH_LINES)
        self.user = self.conf["user"]
        self.password = self.conf["password"]
        self.host = self.conf["host"]
        self.port = self.conf["port"]
        self.database = self.conf["database"]
        self.query = self.conf["query"]
        self.authentication = self.conf.get("authentication", "SqlPassword")
        self.query_timeout = self.conf.get("query_timeout", 60)
        self.output_directory = self.conf["output_directory"]
        # backward compatibility for typo
        self.read_committed = self.conf.get("read_committed", self.conf.get("read_commited"))
        self.txn_isolation = self.conf.get("txn_isolation")
        self.use_tcp = self.conf.get("use_tcp", True)
        self._connection = None
        self._cursor = None

    def load_config(self, config_path):
        """
        Loads the config file.
            :param config_path: Path to the config file (e.g.: "/config/config.json")
        """
        if not os.path.exists(config_path):
            raise Exception("Configuration not specified")

        with open(config_path) as conf_file:
            conf = json.loads(conf_file.read())
            return conf

    def establish_connection(self):
        logger.info("Connecting to MSSQL database...")
        connection_string = ("Driver={{ODBC Driver 18 for SQL Server}};" +
                             "Server={protocol}{host},{port};" +
                             "Database={db};" +
                             "Authentication={authentication};" +
                             "Uid={user};" +
                             "Pwd={password};" +
                             "Encrypt=yes;" +
                             "TrustServerCertificate=yes;" +
                             "Connection Timeout=30;").format(
            host=self.host,
            db=self.database,
            user=self.user,
            password=self.password,
            authentication=self.authentication,
            port=self.port,
            protocol="tcp:" if self.use_tcp else ""
        )

        connection = pyodbc.connect(connection_string, autocommit=(self.read_committed is not None), timeout=30)
        logger.info("Connection established.")
        connection.timeout = self.query_timeout

        if self.read_committed is not None:
            connection.set_attr(pyodbc.SQL_ATTR_TXN_ISOLATION,
                                      pyodbc.SQL_TXN_READ_COMMITTED if self.read_committed else pyodbc.SQL_TXN_READ_UNCOMMITTED)

        if self.txn_isolation is not None:
            if self.txn_isolation in ['committed', 'commited']:  # backward compatibility for typo
                connection.set_attr(pyodbc.SQL_ATTR_TXN_ISOLATION, pyodbc.SQL_TXN_READ_COMMITTED)
            elif self.txn_isolation in ['uncommitted', 'uncommited']:  # backward compatibility for typo
                connection.set_attr(pyodbc.SQL_ATTR_TXN_ISOLATION, pyodbc.SQL_TXN_READ_UNCOMMITTED)
            elif self.txn_isolation == 'repeatable_read':
                connection.set_attr(pyodbc.SQL_ATTR_TXN_ISOLATION, pyodbc.SQL_TXN_REPEATABLE_READ)
            elif self.txn_isolation == 'serializable':
                connection.set_attr(pyodbc.SQL_ATTR_TXN_ISOLATION, pyodbc.SQL_TXN_SERIALIZABLE)
        return connection

    @property
    @RetryManager(max_attempts=10, wait_seconds=5, exceptions=pyodbc.Error)
    def connection(self):
        if self._connection is None:
            self._connection = self.establish_connection()
        return self._connection

    @property
    def cursor(self):
        if self._cursor is None:
            self._cursor = self.connection.cursor()
        return self._cursor

    def close_cursor_and_connection(self):
        logger.info("Closing connection...")
        if self._cursor:
            self._cursor.close()
            self._cursor = None
        if self._connection:
            self._connection.close()
            self._connection = None
        logger.info("Connection closed")

    def create_output_directory(self, dir_path):
        """
        Create target directory & all intermediate directories if don't exists.
            :param dir_path: directory Path (e.g.: "/data/out/tables")
        """
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
            logger.info(f"Directory {dir_path} Created ")
        else:
            logger.info(f"Directory {dir_path} already exists")

    def extract_query(self, file_name, query):
        logger.info(f"Executing query: {query}")
        self.cursor.execute(query)
        field_names = [i[0] for i in self.cursor.description]
        logger.info("Column names: " + str(field_names))

        output_filename = os.path.join(self.output_directory, file_name + ".csv")
        logger.info("Saving output to \'{0}\'".format(output_filename))

        with open(output_filename, 'w', newline='') as result_file:
            wr = csv.writer(result_file, dialect=csv.unix_dialect)
            wr.writerow(field_names)
            buf = self.cursor.fetchmany(self.BATCH_LINES)
            i = 0
            while buf:
                i += 1
                logger.info("Saving batch #{0}".format(i))
                wr.writerows(buf)
                buf = self.cursor.fetchmany(self.BATCH_LINES)
        logger.info("Table output to \'{0}\'".format(output_filename))

    def extract(self):
        self.create_output_directory(self.output_directory)
        for name, query in self.query.items():
            with RetryManager(max_attempts=10, wait_seconds=5, exceptions=pyodbc.Error,
                              reset_func=self.close_cursor_and_connection) as retry:
                while retry:
                    with retry.attempt:
                        self.extract_query(name, query)
        self.close_cursor_and_connection()
