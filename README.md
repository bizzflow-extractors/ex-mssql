# ex-mssql

Extractor for MS SQL database

## Note

*Due to Alpine Linux not being able to run MS SQL ODBC Driver, this container runs CentOS. It may take a little longer to build.*

## Example config

```json
{
    "user": "username",
    "host": "host.address.net",
    "password": "secret.P@ssword",
    "port": 1433,
    "database": "db-name",
    "debug": true,
    "output_directory": "/data/out/tables",
    "query_timeout": 60,
    "query": {
        "tables": "SELECT * FROM INFORMATION_SCHEMA.TABLES",
        "columns": "SELECT * FROM INFORMATION_SCHEMA.COLUMNS"
    },
    "chunk_size": 10000,
    "read_committed": true,  
    "txn_isolation": "committed",
    "authentication": "SqlPassword",
    "use_tcp": true
}
```

- `chunk_size` is not required, default: 10000 lines per chunk
- `read_committed` may be true / false or null. `true` = read committed, `false` = read uncommitted, `null` or omitted = unset  
- `txn_isolation` may be committed / uncommitted / repeatable_read / serializable or null. `committed` = pyodbc.SQL_TXN_READ_COMMITTED, `uncommitted` = pyodbc.SQL_TXN_READ_UNCOMMITTED, `repeatable_read` = pyodbc.SQL_TXN_REPEATABLE_READ, `serializable` = pyodbc.SQL_TXN_SERIALIZABLE, `null` or omitted = unset  
- `authentication` SqlPassword (default) or ActiveDirectoryPassword (for Azure AD login)
- `use_tcp` true/false (true default) add `tcp:` to the host address if true
